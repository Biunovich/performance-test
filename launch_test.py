import subprocess
import time
import random

INSTANCES = 500

procs = []
for i in range(INSTANCES):
    time.sleep(0.125)
    print(i)
    p = subprocess.Popen(['python', 'performance_test.py', str(1+i)])
    procs.append(p)
for i in procs:
    i.wait()
