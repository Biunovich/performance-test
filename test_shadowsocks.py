import sys
import os
import signal
import select
import time
import argparse
from subprocess import Popen, PIPE
DEF_PORT = 1080
client_args = ['ss-local', '-v']
url = 'http://linkedin.com'

def test(Json, port):
    port = DEF_PORT + int(port)
    client_args.extend(['-s', Json['server'], '-p', str(Json['port']), '-l', str(port), '-k', Json['password'], '-m', 'aes-256-cfb'])
    p2 = Popen(client_args, stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)
    p3 = None
    p4 = None
    
    stage = 1
    
    try:
        fdset = []
        time.sleep(2)
        p3 = Popen(['curl', url, '-v', '-L',
            '--socks5-hostname', '127.0.0.1:%s' % str(port),
            '-m', '15', '--connect-timeout', '10'],
            stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)
        if p3 is not None:
            fdset.append(p3.stdout)
            fdset.append(p3.stderr)
            stage = 2
        else:
            return 1
    
        while True:
            r, w, e = select.select(fdset, [], fdset)
            if e:
                break
    
            for fd in r:
                line = fd.readline()
                if not line:
                    if stage == 2 and fd == p3.stdout:
                        stage = 5
    
            if stage == 5:
                r = p3.wait()
                if r != 0:
                    return 1
                print('Shadowsocks test passed')
                return 0
    finally:
        try:
            os.kill(p2.pid, signal.SIGINT)
            os.waitpid(p2.pid, 0)
        except OSError:
            pass
